package org.gcube.application.framework.core.util;

/**
 * Valid service types
 * 
 * @author Nikolas Laskaris 
 *
 */
public enum ServiceType {
	/** simple web service */
	SIMPLE, 
	/** factory service */
	FACTORY,
	/** statefull service - WS-resource */
	STATEFULL
}