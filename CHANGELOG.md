
# Changelog for Catalogue Badge Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v5.1.2] - 2023-02-13

- ported to git


## [v1.0.0] - 2010-12-06

First Release
